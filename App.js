import React, {Component} from 'react';
import {Provider} from "react-redux"
import AppNavigator from "./app/routes"
import store from "./app/store"
import {
    Platform,
    StyleSheet,
    Text,
    View,
    StatusBar
} from 'react-native';

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <AppNavigator />
                </View>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    statusBar: {
        height: StatusBar.currentHeight
    }
});
