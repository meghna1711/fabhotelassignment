### Fabhotel Assignment

The respective repository contains react native Gallery screen assignment. I has a gallery view which show a list of hotel room images and their respective
types.

The tabs are horizontally scrollable with a functionality to update the flat list when changing any specific tabs.

You can also scroll the Flat list and the corresponding type of Tab will be highlighted.

We have used redux as store management tool to capture the respective events and updating the corresponding tabs and list items.

On clicking any image in gallery will take you to Image viewer screen. This screen has a back button to navigate to previous route.

We have used React Navigation's Stack Navigator for navigating between the screen.

Thanks