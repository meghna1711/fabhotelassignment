import React, {Component} from "react"
import {View, Text} from "react-native";
import HeaderComponent from "../../components/common/HeaderComponent"
import {CommonStyles} from "../../utils/Constants"
import TabScrollView from "../../components/TabScrollView"
import RoomsList from "../../components/RoomsList";

export default class Gallery extends Component {
    flatListRef = React.createRef();
    static navigationOptions = ({navigation}) => ({
        header: <HeaderComponent title={"Photo Gallery"}/>
    });

    render() {
        return (
            <View style={CommonStyles.container}>
                <TabScrollView />
                <RoomsList navigation={this.props.navigation}/>
            </View>
        )
    }
}