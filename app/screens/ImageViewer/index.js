import React, {Component} from "react"
import {View, Text, Image, TouchableOpacity} from "react-native";
import Icon from 'react-native-vector-icons/Ionicons';
import {CommonStyles, Colors} from "../../utils/Constants"
import ImageView from "../../components/ImageView"
import styles from "./styles"

export default class ImageViewer extends Component {
    static navigationOptions = ({navigation, header}) => ({
        headerLeft: <TouchableOpacity onPress={() => navigation.goBack()}>
                <Icon name='ios-arrow-back' color={Colors.white} size={25} style={styles.icon}/>
        </TouchableOpacity>,
        headerTitle: navigation.getParam('room').type,
        headerTitleStyle: styles.headerTitleStyle,
        headerStyle : styles.headerStyle
    });

    render() {
        const roomDetails = this.props.navigation.getParam('room');
        return (
            <View style={[CommonStyles.container, styles.mainWrapper]}>
                <ImageView roomDetails={roomDetails}/>
            </View>
        )
    }
}