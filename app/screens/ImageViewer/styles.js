import {StyleSheet} from "react-native"
import {Colors} from "../../utils/Constants"

export default StyleSheet.create({
    mainWrapper: {
        flex: 1,
        backgroundColor: Colors.white,
        justifyContent: "center"
    },
    headerStyle: {
        backgroundColor: Colors.navyBlue,
        borderWidth: 0,
        borderColor: Colors.navyBlue,
        padding: 15
    },
    headerTitleStyle: {
        color: Colors.white,
        fontSize: 18
    },
    icon: {
        padding: 5
    }
})