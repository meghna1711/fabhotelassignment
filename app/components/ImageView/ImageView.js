import React from "react";
import {View, Text, Image} from "react-native";
import styles from "./styles";

export default class ImageView extends React.PureComponent {

    render() {
        const {roomDetails} = this.props;
        return (
            <View styles={styles.mainWrapper}>
                <Image style={styles.image} source={{uri: roomDetails.image}}/>
            </View>
        )
    }
}