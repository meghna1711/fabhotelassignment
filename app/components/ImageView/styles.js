import {StyleSheet, Dimensions} from "react-native"
import {Colors, DeviceDimensions} from "../../utils/Constants"

export default StyleSheet.create({
    image: {
        height: 250,
        width: DeviceDimensions.width,
        alignItems: "center",
    },
    mainWrapper: {
        backgroundColor: "red",
        flex: 1,
        flexDirection: "column",
        alignContent: "center",
        justifyContent: "center"
    }
})