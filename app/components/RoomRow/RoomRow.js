import React from "react";
import {View, Text, Image, TouchableHighlight} from "react-native";
import styles from "./styles";

export default class RoomRow extends React.PureComponent {
    goToImageViewer() {
        this.props.navigation.navigate("ImageViewer", {room: this.props.room})
    }

    render() {
        const {room} = this.props;
        return (
            <View style={styles.roomListWrapper}>
                <TouchableHighlight onPress={this.goToImageViewer.bind(this)}>
                    <Image source={{uri: room.image}} style={styles.roomImage}/>
                </TouchableHighlight>
                <View style={styles.headerWrapper}>
                    <Text style={styles.heading}>{room.type}</Text>
                </View>
            </View>
        )
    }
}