import {StyleSheet, Dimensions} from "react-native";
import {Colors, DeviceDimensions} from "../../utils/Constants"

export default StyleSheet.create({
    headerWrapper: {
        paddingHorizontal: 20,
        paddingVertical: 15,
        backgroundColor: Colors.white
    },
    heading: {
        color: Colors.black
    },
    roomImage: {
        width: DeviceDimensions.width,
        height: 200
    }
})