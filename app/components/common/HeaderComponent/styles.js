import {StyleSheet} from "react-native"
import {Colors, PlatformPadding} from "../../../utils/Constants"

export default StyleSheet.create({
    headerContainer: {
        backgroundColor: Colors.navyBlue,
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        padding: 20,
        paddingTop: PlatformPadding + 20,
    },
    headerText: {
        color: Colors.white,
        fontSize: 20,
        fontWeight: "bold"
    }
})