import React from "react";
import {View, Text} from "react-native";
import styles from "./styles";

export default class HeaderComponent extends React.Component {
    render() {
        const {title} = this.props;
        return (
            <View style={[styles.headerContainer]}>
                <Text style={styles.headerText}>{title}</Text>
            </View>
        )
    }
}