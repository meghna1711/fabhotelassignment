import {StyleSheet, Dimensions} from "react-native"
import {Colors, StatusBarHeight} from "../../utils/Constants"

export default StyleSheet.create({
    tabWrapper: {
        height: 70
    },
    tabBar: {
        height: 50,
        backgroundColor: Colors.navyBlue
    },
    tabBarUnderlineStyle: {
        borderBottomWidth: 2,
        borderBottomColor: Colors.white
    },
    tabBarStyle: {
        color: Colors.white,
        fontSize: 15
    }
})