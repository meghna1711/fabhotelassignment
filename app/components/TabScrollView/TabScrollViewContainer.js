import TabScrollView from "./TabScrollView";
import {connect} from "react-redux";
import actions from "../../store/list/actions";

const mapStateToProps = (state) => {
    const {list} = state;
    const {selectedTabIndex, tabEvent} = list;
    return {
        selectedTabIndex,
        tabEvent
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        highlightActiveRoom: (type) => {
            dispatch(actions.highlightActiveRoom(type))
        },
        updateTabEvent: (value) => {
            dispatch(actions.updateTabEvent(value))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(TabScrollView)