import React from "react";
import {View, Text, Platform} from "react-native";
import styles from "./styles";
import {CommonStyles, Colors} from "../../utils/Constants"
import {RoomCategories} from "../../utils/StaticData"
import ScrollableTabView, {ScrollableTabBar} from 'react-native-scrollable-tab-view';

export default class TabScrollView extends React.Component {
    tabView = React.createRef();

    constructor() {
        super();
        this.state = {
            showAnimation: false
        }
    }

    componentDidMount() {
        setTimeout(() => {
            // This is a known issue in android for the the "react-native-scrollable-tab-view" module so we have used a fix
            if (Platform.OS === "android") {
                this.tabView.goToPage(1);
                this.tabView.goToPage(0);
            }
        }, 500)
    }

    shouldComponentUpdate(nextProps) {
        if (this.props.selectedTabIndex !== nextProps.selectedTabIndex) {
            this.tabView.goToPage(nextProps.selectedTabIndex);
            this.setState({
                showAnimation: false
            })
        }
        return false;
    }

    detectTabChange(tabLabel) {
        this.setState({
            showAnimation: true
        });
        this.props.highlightActiveRoom(tabLabel);
    }

    scrollableTabBar() {
        return <ScrollableTabBar style={styles.tabBar}
                                 scrollWithoutAnimation={!this.props.tabEvent}
                                 capturePress={this.detectTabChange.bind(this)}/>
    }

    render() {
        const {tabEvent} = this.props;
        const {showAnimation} = this.state;
        return (
            <View style={[CommonStyles.containerWithHeader, styles.tabWrapper]}>
                <ScrollableTabView
                    style={{marginTop: 20}}
                    ref={(tabView) => this.tabView = tabView}
                    initialPage={0}
                    tabBarActiveTextColor={Colors.white}
                    tabBarUnderlineStyle={styles.tabBarUnderlineStyle}
                    tabBarTextStyle={styles.tabBarStyle}
                    scrollWithoutAnimation={!showAnimation}
                    renderTabBar={this.scrollableTabBar.bind(this)}>
                    {
                        RoomCategories.map((room, index) => (
                            <Text key={index} tabLabel={room}></Text>
                        ))
                    }
                </ScrollableTabView>
            </View>
        )
    }
}