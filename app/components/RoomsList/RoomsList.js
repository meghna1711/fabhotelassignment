import React from "react";
import {View, Text, FlatList} from "react-native";
import styles from "./styles";
import {RoomListArray} from "../../utils/StaticData";
import RoomRow from "../../components/RoomRow"

const imageHeight = 247;

export default class RoomsList extends React.Component {

    flatListRef = React.createRef();

    constructor() {
        super();
        this.state = {
            scrollToHeight: 0,
            scrolling: false,
            activeRoomType: RoomListArray[0].type
        }
    }

    _keyExtractor = (item, index) => item.id;

    shouldComponentUpdate(nextProps) {
        if (nextProps.activeIndexForRoom !== this.props.activeIndexForRoom && nextProps.tabEvent) {
            this.flatListRef.scrollToIndex({animated: false, index: nextProps.activeIndexForRoom});
        }
        return false;
    }

    _onScroll(event) {
        if (event.nativeEvent.contentOffset.y > 0) {
            let index = Math.floor(event.nativeEvent.contentOffset.y / imageHeight);
            if (RoomListArray[index].type !== this.state.activeRoomType) {
                this.setState({
                    activeRoomType: RoomListArray[index].type
                });
                this.props.highlightActiveTab(RoomListArray[index].type)
            }
            this.setState({
                scrolling: false
            });
        }
    }

    renderRoomRow({item}) {
        return <RoomRow room={item} navigation={this.props.navigation}/>
    }

    render() {
        return (
            <View style={styles.roomListWrapper}>
                <FlatList
                    ref={(ref) => this.flatListRef = ref}
                    data={RoomListArray}
                    extraData={this.state}
                    onScroll={this._onScroll.bind(this)}
                    keyExtractor={this._keyExtractor}
                    renderItem={this.renderRoomRow.bind(this)}
                />
            </View>
        )
    }
}