import RoomsList from "./RoomsList";
import {connect} from "react-redux";
import actions from "../../store/list/actions";

const mapStateToProps = (state) => {
    const {list} = state;
    const {activeIndexForRoom, activeRoomType, tabEvent} = list;
    return {
        activeIndexForRoom,
        activeRoomType,
        tabEvent
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        highlightActiveRoom: (type) => {
            dispatch(actions.highlightActiveRoom(type))
        },
        highlightActiveTab: (type) => {
            dispatch(actions.highlightActiveTab(type))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RoomsList);