import {StyleSheet, Dimensions} from "react-native"
import {Colors, StatusBarHeight} from "../../utils/Constants"

export default StyleSheet.create({
    roomListWrapper: {
        flex: 1
    }
})