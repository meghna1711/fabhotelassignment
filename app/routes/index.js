import {createStackNavigator} from "react-navigation";
import Gallery from "../screens/Gallery";
import ImageViewer from "../screens/ImageViewer";

const routes = createStackNavigator({
    Gallery: Gallery,
    ImageViewer: ImageViewer
}, {
    initialRouteName: "Gallery"
});

export default routes;