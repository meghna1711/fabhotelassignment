import createSagaMiddleware from 'redux-saga';
import {createStore, applyMiddleware, combineReducers} from "redux"
import logger from 'redux-logger';
import ListModule from "./list";

const sagas = [];
const reducers = combineReducers({
    list: ListModule.reducer
});
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducers,
    applyMiddleware(
        logger,
        sagaMiddleware
    )
);

sagas.forEach((saga) => {
    sagaMiddleware.run(saga)
});

export default store;