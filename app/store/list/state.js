import {RoomListArray} from "../../utils/StaticData"

export default {
    roomList: RoomListArray,
    offset: 0,
    limit: 100,
    activeRoomType: "Main",
    activeIndexForRoom: 0,
    selectedTabIndex: 0,
    tabEvent: false,
    activeRoomDetails: {
        type: "Main",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg"
    }
}