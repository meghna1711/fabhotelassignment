import * as actions from "./actions"
import defaultState from "./state"
import {RoomListArray, RoomCategories} from "../../utils/StaticData"

const list = (state = defaultState, action) => {
    switch (action.type) {
        case actions.HIGHLIGHT_ACTIVE_ROOM_CATEGORY: {
            let activeRoomTypeIndex = RoomListArray.findIndex((room) => room.type === action.roomType);
            return {...state, activeRoomType: action.roomType, activeIndexForRoom: activeRoomTypeIndex, tabEvent: true}
        }
            break;
        case actions.HIGHLIGHT_ACTIVE_TAB: {
            let activeTabIndex = RoomCategories.findIndex((room) => room === action.roomType);
            return {...state, activeRoomType: action.roomType, selectedTabIndex: activeTabIndex, tabEvent: false}
        }
            break;
        case actions.UPDATE_TAB_EVENT: {
            return {...state, tabEvent: action.value}
        }
            break;
    }
    return state;
};

export default list;