export const ADD_ROOMS_TO_LIST = "ADD_ROOMS_TO_LIST";
export const UPDATE_ROOMS_LIST = "UPDATE_ROOMS_LIST";
export const HIGHLIGHT_ACTIVE_ROOM_CATEGORY = "HIGHLIGHT_ACTIVE_ROOM_CATEGORY";
export const HIGHLIGHT_ACTIVE_TAB = "HIGHLIGHT_ACTIVE_TAB";
export const UPDATE_TAB_EVENT = "UPDATE_TAB_EVENT";

export default {
    addRoomsToList: () => ({
        type: "ADD_ROOMS_TO_LIST"
    }),
    updateRoomsList: (newItems) => ({
        type: UPDATE_ROOMS_LIST,
        newItems
    }),
    highlightActiveRoom: (roomType) => ({
        type: HIGHLIGHT_ACTIVE_ROOM_CATEGORY,
        roomType
    }),
    highlightActiveTab: (roomType) => ({
        type: HIGHLIGHT_ACTIVE_TAB,
        roomType
    }),
    updateTabEvent: (value) => ({
        type: UPDATE_TAB_EVENT,
        value
    })
}