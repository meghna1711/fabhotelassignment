export const RoomCategories = [
    "Main",
    "Room",
    "Sitting Area",
    "Swimming Pool",
    "Bathroom",
    "Reception",
    "Other",
    "Lobby-corridor",
    "Restaurant",
    "Banquet Hall",
    "Delux Room",
    "Executive Room"
];

export const RoomListArray = [
    {
        id: "1",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Main"
    },
    {
        id: "2",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054814.JPG",
        type: "Main"
    },
    {
        id: "3",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054824.JPG",
        type: "Main"
    },
    {
        id: "4",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054828.JPG",
        type: "Room"
    },
    {
        id: "5",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Room"
    },
    {
        id: "6",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Room"
    }, {
        id: "7",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054838.JPG",
        type: "Sitting Area"
    }, {
        id: "8",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054833.JPG",
        type: "Sitting Area"
    }, {
        id: "9",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054845.JPG",
        type: "Swimming Pool"
    }, {
        id: "10",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054853.JPG",
        type: "Bathroom"
    }, {
        id: "11",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054913.JPG",
        type: "Reception"
    },
    {
        id: "12",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Reception"
    },
    {
        id: "13",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054814.JPG",
        type: "Other"
    },
    {
        id: "14",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054824.JPG",
        type: "Lobby-corridor"
    },
    {
        id: "15",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054828.JPG",
        type: "Lobby-corridor"
    },
    {
        id: "16",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054828.JPG",
        type: "Restaurant"
    },
    {
        id: "17",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Restaurant"
    },
    {
        id: "18",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Banquet Hall"
    }, {
        id: "19",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054838.JPG",
        type: "Banquet Hall"
    }, {
        id: "20",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054833.JPG",
        type: "Banquet Hall"
    },
    {
        id: "21",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Delux Room"
    },
    {
        id: "22",
        image: "https://pimg.fabhotels.com/propertyimages/597/main/main-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180307042451.jpg",
        type: "Delux Room"
    }, {
        id: "23",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054838.JPG",
        type: "Delux Room"
    }, {
        id: "24",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054833.JPG",
        type: "Executive Room"
    }, {
        id: "25",
        image: "https://pimg.fabhotels.com/propertyimages/597/medium/room-photos-fabhotel-ashvem-beach-resort-goa-Hotels-20180306054845.JPG",
        type: "Executive Room"
    },
];