import {Dimensions, StatusBar, StyleSheet, Platform} from "react-native"

export const Colors = {
    white: "#FFFFFF",
    navyBlue: "#1e254a",
    black: "#000000"
};

export const StatusBarHeight = StatusBar.currentHeight;
export const DeviceDimensions = {
    height: Dimensions.get("window").height,
    width: Dimensions.get("window").width,
};

export const PlatformPadding = Platform.OS === "ios" ? 20 : 0;

export const CommonStyles = StyleSheet.create({
    container: {
        backgroundColor: Colors.white,
        flex: 1
    },
    platformPadding: {
        paddingTop: Platform.OS === "ios" ? 20 : 0
    },
    containerWithHeader: {
        backgroundColor: Colors.white,
        marginTop: Platform.OS === "ios" ? 60 : 40
    }
});